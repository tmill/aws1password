# aws1password

Simple script to populate the **~/.aws/credentials** file with credentials saved in 1Password.

## Requirements

1. Bash or zsh shell
2. Needs bash to run the script
3. 1Password cli (op or op.exe in wsl)
4. Integration with the 1Password CLI enabled.
5. 1Password item needs to have the following fields:
   1. Access Key ID
   2. Secret Access Key  
   
![](exemplo.png "Exemplo")


## Atention!

If a **~/.aws/credentials** file already exists, please back it up and remove it.  
The script will not execute if there is a credentials file that was not generated by it.

## How to use it

Run the script, passing as a parameter the name of the item in 1Password:

```
aws1password.sh "Exemplo"
```

## What is done

The script performs the following:

1. Retrieves the item data from 1Password
2. Inserts the data into the **~/.aws/credentials** file in the appropriate format.
3. Creates the **~/.bash_logout** or **~/.zlogout** file if it doesn't exist and add a snippet to remove the **~/.aws/credentials** file if it was created by the script
4. Add a snippet to the **~/.bashrc** or **~/.zshrc** file to run the logout script if the window/tab is closed at a terminal



## Creating a profile in the config file (assume-role)
Use the following example to add an assume-role profile to the **~/.aws/config** file:
```
[profile example]
role_arn = arn:aws:iam::example_id_account:role/example_role
source_profile = default
```