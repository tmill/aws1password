#!/bin/bash


logout_script(){

case "${1}" in
	bash)
		logout_file="$HOME/.bash_logout"
		rc_file="$HOME/.bashrc"
	;;
	zsh)
		logout_file="$HOME/.zlogout"
		rc_file="$HOME/.zshrc"
	;;
	*)
		true
	;;
esac

if [[ -f $logout_file ]];then
		$sed_bin -i '/# Snippet created by aws1password script/,+4d' "$logout_file"
fi

cat << EOF >> "$logout_file"
# Snippet created by aws1password script
if ( grep "# Managed by aws1password script" ~/.aws/credentials > /dev/null );then
	rm -f ~/.aws/credentials
fi
EOF

$sed_bin -i '/# Snippet created by aws1password script/,+5d' "$rc_file"
cat << EOF >> "$rc_file"
# Snippet created by aws1password script
exit_session() {
    source "$logout_file"
}
trap exit_session SIGHUP
EOF

}

if ( uname -a | grep WSL > /dev/null );then
	op_bin=op.exe
else
	op_bin=op
fi

if [[ $(uname -s) == 'Darwin' ]];then
	sed_bin="gsed"
else
	sed_bin="sed"
fi

if ( ! $sed_bin --version >/dev/null 2>&1 );then
	echo "$sed_bin not installed"
	exit 1
fi

if ( ! $op_bin -v >/dev/null 2>&1 );then
	echo "$op_bin not installed"
	exit 1
fi

[[ -d ~/.aws ]] || mkdir ~/.aws

if [ -f ~/.aws/credentials ]; then
	if ( ! grep "# Managed by aws1password script" ~/.aws/credentials > /dev/null );then
		echo "The credentials file exists and was not created by this script. It needs to replace it." 
		echo "Please consider making a backup of your credentials file and removing it before using this script."
		exit 1
	fi
	echo -n "The credentials file already exists in ~/.aws/credentials. Would you like to replace it? (y/n): "
	read -r choice
	if [[ $choice == "y" ]];then
		rm -f ~/.aws/credentials
    else
	    echo "File not removed"
	    exit 0
	fi
fi


CRED=$($op_bin item get "$1" --fields "label=Access Key ID,label=Secret Access Key")
AK=$(echo $CRED | cut -d, -f1)
SK=$(echo $CRED | cut -d, -f2)

cat << EOF > ~/.aws/credentials
# Managed by aws1password script
[default]
aws_access_key_id=$AK
aws_secret_access_key=$SK
EOF

chmod 400 ~/.aws/credentials

interpreter=$(basename "$SHELL")

if [[ "${interpreter}" == "bash" ]]; then
	logout_script bash
elif [[ "${interpreter}" == "zsh" ]]; then
	logout_script zsh
fi

if [ ! -f "$HOME/.aws/.aws1password-firstrun-ok" ]; then
	touch "$HOME/.aws/.aws1password-firstrun-ok"
	echo "Please close this terminal session to finish the configuration"
fi

